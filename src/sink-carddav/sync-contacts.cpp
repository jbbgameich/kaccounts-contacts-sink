/*************************************************************************************
 *  Copyright (C) 2019 by Rituka Patwal <ritukapatwal21@gmail.com>                   *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#include <sink/store.h>
#include <sink/secretstore.h>
#include <sink/resourcecontrol.h>
#include "sync-contacts.h"
#include <QDebug>
#include <QUrl>
#include <QUuid>

#include <Accounts/Service>
#include <Accounts/Manager>
#include <Accounts/Account>
#include <Accounts/AccountService>

#include "getcredentialsjob.h"
#include "core.h"

SyncContacts::SyncContacts()
{
}

SyncContacts::SyncContacts(quint32 accountId, QString server, QString userName, QString password)
    :   m_accountId(accountId), 
        m_server(server),
        m_userName(userName),
        m_password(password)
{
}

SyncContacts::~SyncContacts()
{
}

void SyncContacts::createResource(){ 

    Accounts::Account *account = KAccounts::accountsManager()->account(m_accountId);

    if(account->value("resourceId").toString() != ""){
        QByteArray b;
        b.setNum(m_accountId);
        auto resource = Sink::ApplicationDomain::CardDavResource::create(b);
        resource.setProperty("server", m_server);
        resource.setProperty("username", m_userName);
        auto resourceId = resource.identifier();

        Sink::SecretStore::instance().insert(resourceId, m_password);
        Store::create(resource).exec().waitForFinished();
        qDebug()<<"***CONTACT CREATED***";

        account->setValue("resourceId", resourceId);
    }
    m_resourceInstanceIdentifier = account->value("resourceId").toString().toUtf8();
    synchContact();

    return;
}

void SyncContacts::synchContact()
{
    //start resourcecontrol
    Sink::ResourceControl::start(m_resourceInstanceIdentifier);

    //Sync Addressbooks
    Sink::SyncScope scope1;
    scope1.setType<Addressbook>();
    scope1.resourceFilter(m_resourceInstanceIdentifier);
    Store::synchronize(scope1).exec().waitForFinished();

    //flush
    Sink::ResourceControl::flushMessageQueue(m_resourceInstanceIdentifier).exec().waitForFinished();

    //Sync Contacts
    Sink::SyncScope scope2;
    scope2.setType<Sink::ApplicationDomain::Contact>();
    scope2.resourceFilter(m_resourceInstanceIdentifier);
    Store::synchronize(scope2).exec().waitForFinished();

    //flush
    Sink::ResourceControl::flushMessageQueue(m_resourceInstanceIdentifier).exec().waitForFinished();
    
    return;
}